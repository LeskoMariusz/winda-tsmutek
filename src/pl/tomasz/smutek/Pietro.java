package pl.tomasz.smutek;

import java.util.ArrayList;
import java.util.List;

public class Pietro {

    private int nrPietra;
    private List<Zadanie> listZadan = new ArrayList<>();

    public Pietro(int nrPietra) {
        this.nrPietra = nrPietra;
    }

    public int getNrPietra() {
        return nrPietra;
    }

    public List<Zadanie> getListZadan() {
        return listZadan;
    }

    public void dodajZadanie(int docelowePietro) {
        listZadan.add(new Zadanie(nrPietra, docelowePietro));
    }

    public boolean czyMaszZadaniaJazdyWKierunku(Kierunek kierunek) {
        return listZadan.stream().anyMatch(zadanie -> zadanie.getKierunek() == kierunek);
    }


    public List<Zadanie> usunZadaniaZgodneZKierunkiem(Kierunek kierunek) {

        List<Zadanie> result = new ArrayList<>();
        Zadanie zadanie;
        for (int i = listZadan.size() - 1; i >= 0; i--){
            zadanie = listZadan.get(i);
            if (zadanie.getKierunek() == kierunek){
                result.add(zadanie);
                listZadan.remove(zadanie);
            }
        }

        return result;
    }
}
