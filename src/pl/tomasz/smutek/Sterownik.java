package pl.tomasz.smutek;

import java.util.ArrayList;
import java.util.List;

public class Sterownik {

    private Winda winda;
    private List<Pietro> listaPieter = new ArrayList<>();

    public Sterownik(Winda winda) {
        this.winda = winda;
    }

    public void dodajPietro(int pietro) {
        listaPieter.add(new Pietro(pietro));
    }

    public void dodajPietro(Pietro pietro) {
        listaPieter.add(pietro);
    }

    public int liczbaPieter() {
        return listaPieter.size();
    }

    public void zrobKrok() {
        /*
        I KROK
        Sprawdzamy czy w windzie jest jakieś zadanie
        dojechania na piętro na którym obecnie jest winda
            Jeżeli tak, to wyrzucamy te zadania z windy
            Jeżeli nie, to idż do II KROK
*/
        if (winda.czyZatrzymacWinde()) {
            winda.usunZadania();

            //

            return;
        }

/*        II KROK
        Sprawdzamy czy na aktualnym pietrze z windy istnieje zadanie
        jechania w kierunku zgodnym z kierunkiem windy.
            Jeżeli tak to wrzuć zadania do windy
            Jeżeli nie to idź do III KROK
*/

        Pietro aktualnePietroWindy = listaPieter.get(winda.getAktualnePietro());

        if (aktualnePietroWindy.czyMaszZadaniaJazdyWKierunku(winda.getKierunek())) {

            List<Zadanie> doWindy = aktualnePietroWindy.usunZadaniaZgodneZKierunkiem(winda.getKierunek());
            winda.dodajZadania(doWindy);

            return;
        }

        /*
         III KROK

         Zmieniamy aktualne piętro windy na to zgodne z jej kierunkiem.
         */
        int indexPietra = listaPieter.indexOf(aktualnePietroWindy);
//        indexPietra = winda.getKierunek() == Kierunek.GORA ?
//                indexPietra++ :
//                winda.getKierunek() == Kierunek.DOL ? indexPietra-- : indexPietra;
        switch (winda.getKierunek()) {
            case GORA:
                indexPietra++;
                break;
            case DOL:
                indexPietra--;
                break;
        }
        if (indexPietra < 0 || indexPietra >= listaPieter.size()){
//            throw new IndexOutOfBoundsException();
            System.out.println("Winda chciała wejść na indeks: " + indexPietra);
            // czyszczenie błędnych zadań
            winda.setKierunek(Kierunek.STOP);
        } else {
            winda.setAktualnePietro(listaPieter.get(indexPietra));
        }
    }

//    public Pietro dajPietro(int nrPietra) {
//        return listaPieter.get(nrPietra);
//    }
}
