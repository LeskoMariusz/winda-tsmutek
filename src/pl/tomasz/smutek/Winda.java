package pl.tomasz.smutek;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Winda {

    List<Zadanie> listaZadan = new ArrayList<>();
    Kierunek kierunek = Kierunek.STOP;
    int aktualnePietro = 0;

    public int getAktualnePietro() {
        return aktualnePietro;
    }

    public void usunZadania() {
        Zadanie zadanie;
        for (int index = listaZadan.size() - 1; index >= 0; index--) {
            zadanie = listaZadan.get(index);
            if (zadanie.czyDocelowe(aktualnePietro)) {
                listaZadan.remove(index);
            }
        }
    }
    // odpowiedzieć czy na aktualnym pietrze jest kogo wysadzić

    public boolean czyZatrzymacWinde() {
        return listaZadan.stream().anyMatch(zadanie -> zadanie.czyDocelowe(aktualnePietro));
    }

    public boolean czyPusta() {
        return listaZadan.isEmpty();
    }

    @Override
    public String toString() {
        return "WINDA: aktualnePietro= " + aktualnePietro +
                ", kierunek= " + kierunek +
                ", listaZadan= " + listaZadan;
    }

    public Kierunek getKierunek() {
        return kierunek;
    }

    public void setKierunek(Kierunek kierunek) {
        this.kierunek = kierunek;
    }

    public void dodajZadania(List<Zadanie> doWindy) {
        doWindy.stream().forEach(zadanie -> listaZadan.add(zadanie));
    }

    public void setAktualnePietro(Pietro aktualne) {
        this.aktualnePietro = aktualne.getNrPietra();
    }
}
