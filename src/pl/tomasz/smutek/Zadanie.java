package pl.tomasz.smutek;

public class Zadanie {
    // na to jedziemy
    private int pietroDo;
    // z tego jedziemy
    private int pietroZ;

    Kierunek kierunek;

    public Zadanie(int pietroDo, int pietroZ) {
        this.pietroDo = pietroDo;
        this.pietroZ = pietroZ;

        kierunek = pietroDo > pietroZ ? Kierunek.GORA : Kierunek.DOL;
    }

    public int getPietroDo() {
        return pietroDo;
    }

    public int getPietroZ() {
        return pietroZ;
    }

    public Kierunek getKierunek() {
        return kierunek;
    }

    /**
     * Metoda ma na celu zwrócić informacje czy pietro zadane parametrem
     * jest docelowe dla tego zadania
     *
     * @param pietro
     * @return
     */
    public boolean czyDocelowe(int pietro) {
        return pietro == pietroDo;
    }
}
